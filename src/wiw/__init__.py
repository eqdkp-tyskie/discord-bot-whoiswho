import discord
import requests
import traceback
from decouple import config
from discord import app_commands
from tabulate import tabulate


def get_char_data(eqdkp_base_url, api_token, char_name, eqdkp_plus_multidkp_id=1):
    data = [
        "function=whoiswho",
        "format=json",
        f"name={char_name}",
        f"multidkpid={eqdkp_plus_multidkp_id}",
    ]
    url = f"{eqdkp_base_url}/api.php?{'&'.join(data)}"

    response = requests.get(
        url,
        headers={
            "Authorization": f"{api_token}",
            "User-Agent": "WhoIsWhoBot/v2",
        },
    )
    return response.json()


def get_table(data):
    table = []
    for char_name, char_data in data.get("characters").items():
        name = "%s (main)" % char_name if char_data.get("is_main") else char_name
        table.append(
            [
                name,
                char_data.get("level"),
                char_data.get("class"),
                char_data.get("race"),
            ]
        )
    return table


class MyClient(discord.Client):
    def __init__(self, *, intents: discord.Intents, guild):
        super().__init__(intents=intents)
        self.tree = app_commands.CommandTree(self)
        self.guild = guild

    async def setup_hook(self):
        self.tree.copy_global_to(guild=self.guild)
        await self.tree.sync(guild=self.guild)


guild = discord.Object(id=config("WIW_DISCORD_GUILD_NAME", cast=int))
intents = discord.Intents.default()
intents.message_content = True
client = MyClient(intents=intents, guild=guild)


@client.tree.command()
@app_commands.describe(
    char_name="The name of the char to lookup",
)
async def whoiswho(interaction: discord.Interaction, char_name: str):
    eqdkp_plus_token = config("WIW_EQDKP_API_TOKEN", cast=str)
    eqdkp_plus_base_url = config("WIW_EQDKP_BASE_URL", cast=str)
    eqdkp_plus_multidkp_id = config("WIW_EQDKP_MULTIDKP_ID", cast=str)
    try:
        data = get_char_data(
            eqdkp_plus_base_url, eqdkp_plus_token, char_name, eqdkp_plus_multidkp_id
        )
    except Exception as e:
        print(e)
        print(traceback.format_exc())
        return await interaction.response.send_message(
            f"```Error: {e}```", ephemeral=True
        )

    if not data.get("characters"):
        return await interaction.response.send_message(
            f"```Error: {data}```", ephemeral=True
        )

    table = get_table(data)
    await interaction.response.send_message(
        f"```Points: {data.get('points')}, list"
        + f"of chars:\n{tabulate(table, tablefmt='grid')}\n```",
        ephemeral=True,
    )
