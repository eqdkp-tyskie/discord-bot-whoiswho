import click
from decouple import config

from wiw import client


@click.command()
def main():
    token = config("WIW_DISCORD_TOKEN")
    client.run(token)


if __name__ == "__main__":
    main()
