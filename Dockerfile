ARG USERNAME="discord-bot"
ARG PROJECT_DIR="."

# Base image
FROM python:3.11-slim as base
ARG USERNAME
RUN apt update && \
    apt upgrade -y && \
    apt install sudo gcc git telnet curl -y && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
    rm -rf /var/lib/apt/lists/*

RUN useradd \
    --create-home \
    -u 1000 \
    --home-dir /home/${USERNAME} \
    --shell /bin/bash \
    --groups sudo \
    ${USERNAME}

USER ${USERNAME}
RUN python3 -m venv /home/${USERNAME}/venv
ENV PATH="/home/${USERNAME}/venv/bin:$PATH"
RUN /home/${USERNAME}/venv/bin/pip install -U pip setuptools wheel

# Build the wheels
FROM base as wheeler
ARG PROJECT_DIR
ARG USERNAME
COPY --chown=${USERNAME}:${USERNAME} ${PROJECT_DIR} /home/${USERNAME}/app/
WORKDIR /home/${USERNAME}/app
ENV PYTHONDONTWRITEBYTECODE=1
RUN pip wheel . -w /home/${USERNAME}/wheels

# The project image
FROM base
ARG USERNAME
COPY --from=wheeler /home/${USERNAME}/wheels /home/${USERNAME}/wheels
RUN pip install /home/${USERNAME}/wheels/* \
    && sudo rm -rf /home/${USERNAME}/wheels/
WORKDIR /home/${USERNAME}/

CMD ["/home/discord-bot/venv/bin/eqdkp-whoiswho-bot"]
