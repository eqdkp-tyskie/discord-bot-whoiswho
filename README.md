# Install
You will require Python 3+

```
pip install -r requirements.txt
```

# Configuration
Create a .env file and fill with configuration:

```
WIW_DISCORD_TOKEN=""
WIW_DISCORD_GUILD_NAME=""
WIW_EQDKP_API_TOKEN=""
WIW_EQDKP_BASE_URL=""
WIW_EQDKP_MULTIDKP_ID=""
```

# Run

```
python bot.py
```