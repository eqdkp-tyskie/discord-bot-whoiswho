#!/usr/bin/env bash
set -e -u -o pipefail

pip install -e ".[tests]"
